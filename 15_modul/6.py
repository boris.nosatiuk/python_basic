#Задача 6. Анализ слова

word = input('Введите слово: ')
word_list = list(word)
count_unique = 0

for letter in word:
    count_letter = 0
    for element in word_list:
        if letter == element:
            count_letter += 1
    if count_letter == 1:
        count_unique += 1

print('Количество уникальных букв: ', count_unique)

