#Задача 5. Кино

films = ['Крепкий орешек', 'Назад в будущее', 'Таксист', 'Леон', 'Богемская рапсодия', 'Город грехов', 'Мементо', 'Отступники', 'Деревня']
favourite_film = []

count = int(input('Сколько фильмов хотите добавить? '))

for _ in range(count):
    print('Введите название фильма:', end=' ')
    name_film = input()
    if name_film in films:
        favourite_film.append(name_film)
    if name_film not in films:
        print('Ошибка: фильма', name_film, ' у нас нет :(')

print('\nВаш список любимых фильмов: ', end='')
for i in favourite_film:
    print(i, end=', ')