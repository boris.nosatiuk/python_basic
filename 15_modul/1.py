#Задача 1. Список из нечётных чисел от 1 до N

numbers_list = []

number = int(input('Введите число: '))

for i in range(1, number + 1, 2):
    numbers_list.append(i)

print('Cписок из нечётных чисел от одного до N: ', numbers_list)