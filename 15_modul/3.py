#Задача 3. Клетки

cage_list = []
number_cage = int(input('Количество клеток: '))

for i in range(number_cage):
    print('Эффективность', i+1, 'клетки:', end=' ')
    efficiency_level = int(input())
    cage_list.append(efficiency_level)

print('Неподходящие значения:', end=' ')

for i in range(1, number_cage+1):
    if i > cage_list[i-1]:
        print(cage_list[i-1], end=' ')