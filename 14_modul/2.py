#Задача 2. Сессия

print("Введите первую точку")
x1 = float(input('X: '))
y1 = float(input('Y: '))
print("\nВведите вторую точку")
x2 = float(input('X: '))
y2 = float(input('Y: '))

x_diff = x1 - x2
y_diff = y1 - y2
if x1 == x2:
    print('\nФункции проходящей эти точки не существует. Уравнение имеет вид:', ' x =', x1)
elif y1 == y2:
    print("\nУравнение прямой, проходящей через эти точки:")
    print("y = ", y1)
else:
    k = y_diff / x_diff
    b = y2 - k * x2

    print("\nУравнение прямой, проходящей через эти точки:")
    print("y = ", k, " * x + ", b)