#Задача 6. Монетка 2

import math

def finding(x, y, radius):
    if math.sqrt(x**2 + y**2) <= radius:
        print('\nМетка где-то рядом')
    else:
        print('\nМонетки в области нет')

print('Введите координаты монетки:')

x = float(input('X: '))
y = float(input('Y: '))
radius = int(input('Введите радиус: '))

print(finding(x, y, radius))