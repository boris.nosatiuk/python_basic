#Задача 4. Число наоборот 3

def whole_part(number):
    reverse_num = 0
    while number > 0:
        digit = number % 10
        number = number // 10
        reverse_num = reverse_num * 10
        reverse_num = reverse_num + digit
    return reverse_num

def fractional_part(number):
    fraction_part = round((number - int(number)), 10)
    count = 0
    while fraction_part - int(fraction_part) != 0:
        fraction_part *= 10
        count += 1
    revers_fractional_part = whole_part(fraction_part) * 10 ** (-count)
    return revers_fractional_part


number1 = float(input('\nВведите первое число: '))
number2 = float(input('Введите второе число: '))

reverse_number1 = whole_part(int(number1)) + fractional_part(number1)
reverse_number2 = whole_part(int(number2)) + fractional_part(number2)

print('\nПервое число наоборот:', reverse_number1)
print('Второе число наоборот:', reverse_number2)
print('Сумма: ', reverse_number1 + reverse_number2)